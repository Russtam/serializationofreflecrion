﻿using SerializationApp.BL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace SerializationApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var path = @"C:\\Work1\TestFiles";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                ISerializer<Person> jsonSerializer = new JSONSerialize<Person>(path);
                var person = new Person("Mike", "Yekaterinburg", 23);
                Console.WriteLine("JsonSerialize\n" + string.Join("\n", TimeMeas(jsonSerializer, person)));
                Console.WriteLine();
                ISerializer<Person> csvSerializer = new CSVSerialize<Person>(path);

                Console.WriteLine("CSVSerialize\n" + string.Join("\n", TimeMeas(csvSerializer, person)));
                Console.WriteLine();
                ISerializer<Person> xmlSerializer = new XMLSerialize<Person>(typeof(Person), path);
                Console.WriteLine("XMLSerialize\n" + string.Join("\n", TimeMeas(xmlSerializer, person)));

                var watch = new Stopwatch();
                Console.WriteLine("CSVDeserialize");
                Console.WriteLine(watch.ElapsedMilliseconds.ToString());
                watch.Start();
                var csvfullName = $"{path}\\CsvData.csv";
                if (IsFileExists(csvfullName))
                {
                    var csvPerson = csvSerializer.Deserialize(csvfullName);
                }
                watch.Stop();
                Console.WriteLine(watch.ElapsedMilliseconds.ToString());
                watch.Reset();
                Console.WriteLine("JsonDeserialize");
                Console.WriteLine(watch.ElapsedMilliseconds.ToString());
                watch.Start();
                var jsonFullName = $"{path}\\jsonFile.json";
                if (IsFileExists(jsonFullName))
                {
                    var jsonPerson = jsonSerializer.Deserialize(jsonFullName);
                }
                watch.Stop();
                Console.WriteLine(watch.ElapsedMilliseconds.ToString());

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            Console.ReadKey();
        }

        private static bool IsFileExists(string fullName)
        {
            if (!Directory.Exists(fullName))
                return false;
            return true;
        }

        public static IEnumerable<string> TimeMeas(ISerializer<Person> serializer, Person person)
        {
            var resultList = new List<string>();
            var watch = new Stopwatch();
            resultList.Add(watch.ElapsedMilliseconds.ToString());
            watch.Start();
            if (person == null)
            {
                watch.Stop();
                throw new ArgumentNullException();
            }
            resultList.Add(serializer.Serialize(person));
            watch.Stop();
            resultList.Add(watch.ElapsedMilliseconds.ToString());
            return resultList;
        }
    }
}
