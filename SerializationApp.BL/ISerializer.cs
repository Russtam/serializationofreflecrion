﻿using System;

namespace SerializationApp.BL
{
    public interface ISerializer<T>
    {
        string Serialize(T serializeObj);
        T Deserialize(string path);
    }
}
