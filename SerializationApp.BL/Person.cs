﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerializationApp.BL
{
    public class Person
    {
        public Person() { }
        public Person(string name, string address, int age)
        {
            Name = name;
            Address = address;
            Age = age;
            i1 = 1; i2 = 2; i3 = 3; i4 = 4; i5 = 5;
        }
        public int i1, i2, i3, i4, i5;
        
        public string Name { get;  set; }
        public string Address { get;  set; }
        public int Age { get;  set; }

    }
}
