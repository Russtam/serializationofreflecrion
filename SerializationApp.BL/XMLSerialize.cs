﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace SerializationApp.BL
{
    public class XMLSerialize<T> : ISerializer<T> where T : class
    {
        private readonly FileStream _stream;
        private readonly Type _type;
        public XMLSerialize() { }
        public XMLSerialize(Type type, string path)
        {
            _stream = new FileStream($"{ path }\\xmlFile.xml", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            _type = type;
        }
        public T Deserialize(string text)
        {
            XmlSerializer serializer = new XmlSerializer(_type);
            try
            {
                var xmlRes = (T)serializer.Deserialize(_stream);
                _stream.Close();
                return xmlRes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Serialize(T serializeObj)
        {
            XmlSerializer serializer = new XmlSerializer(_type);
                serializer.Serialize(_stream, serializeObj);
            _stream.Close();
            return null;
        }
    }
}
