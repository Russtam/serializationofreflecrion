﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SerializationApp.BL
{
    public class CSVSerialize<T> : ISerializer<T> where T : new()
    {
        private readonly FileStream _stream;
        public CSVSerialize(string path)
        {
            _stream = new FileStream($"{path}\\CsvData.csv", FileMode.OpenOrCreate, FileAccess.Write);
        }
        public T Deserialize(string path)
        {
            var deserializeObj = File.ReadAllText(path);
            var person = new T();
            var membersArr = deserializeObj.Split(';');
            var type = typeof(T);
            var dict = membersArr.Select(item =>
            {
                var itemArr = item.Split('=').Select(x => x.Trim()).ToArray();
                if (itemArr.Length < 2)
                    return KeyValuePair.Create<string, string>(null, null);
                var pair = KeyValuePair.Create(itemArr[0], itemArr[1]);
                return pair;
            });
            foreach (var item in dict)
            {
                if (item.Key != null)
                {
                    var member = type.GetMember(item.Key).Single();

                    if (member is PropertyInfo)
                    {
                        var prop = member as PropertyInfo;
                        if (item.Value != null)
                            prop.SetValue(person, Convert.ChangeType(item.Value, prop.PropertyType), null);
                    }
                    else if (member is FieldInfo)
                    {
                        var field = member as FieldInfo;
                        if (item.Value != null)
                            field.SetValue(person, Convert.ChangeType(item.Value, field.FieldType));
                    }
                }
            }
            return person;
        }


        public string Serialize(T serializeObj)
        {
            try
            {
                var type = serializeObj.GetType();
                var props = type.GetProperties().Select(x => $"{x.Name}={x.GetValue(serializeObj)}");
                var fields = type.GetFields().Select(x => $"{x.Name}={x.GetValue(serializeObj)}");
                var list = new List<string>();
                if (props.Count() != 0)
                    list.AddRange(props);
                if (fields.Count() != 0)
                    list.AddRange(fields);
                if (list.Count == 0)
                    throw new Exception("Объект не может быть сериализован, по причине отсутствия данных");
                var byteArr = Encoding.Default.GetBytes(string.Join(";", list) + "\r\n");
                _stream.Write(byteArr);
                _stream.Close();
                return string.Join(" ", list);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
