﻿
using System.IO;
using System.Text;
using System.Text.Json;

namespace SerializationApp.BL
{
    public class JSONSerialize<T> : ISerializer<T> where T: new()
    {
        private FileStream _stream;
        public JSONSerialize(string path)
        {
            _stream = new FileStream($"{ path }\\jsonFile.json", FileMode.OpenOrCreate, FileAccess.ReadWrite);
        }
        public T Deserialize(string path)
        {
            var text = File.ReadAllText(path);
            if (string.IsNullOrWhiteSpace(text)||string.IsNullOrEmpty(text))
            {
                return default;
            }
            return JsonSerializer.Deserialize<T>(text);
        }

        public string Serialize(T serializeObj)
        {
            var json = JsonSerializer.Serialize(serializeObj);
            var byteArr = Encoding.Default.GetBytes(json);
            _stream.Write(byteArr);
            _stream.Close();
            return json;
        }
    }
}
